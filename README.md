Cách build dự án chơi cờ caro online.

// Chạy ở môi trường local:
Đứng tại folder BE
Chạy lệnh: npm install, để kéo thư viện
Sau khi kéo thư viện thành công.
Chạy lệnh npm run develop, để chạy source với môi trường development

Truy cập đường dẫn sau để sửa thông số database: BE/config/environments/development/database.json

Sau khi start được soure code.
Truy cập đường dẫn sau để có thể viết script trên front-end: BE/public/index.html

Chạy url trên trình duyệt web để thấy được website hoạt động: http://localhost:1337
Truy cập vào trang admin để quản lý: http://localhost:1337/admin


// Chạy ở môi trường production:
Đứng tại folder BE
Chạy lệnh: npm install, để kéo thư viện
Sau khi kéo thư viện thành công.
Chạy lệnh "NODE_ENV=production node server.js", để chạy source với môi trường production

Truy cập đường dẫn sau để sửa thông số database: BE/config/environments/production/database.json
Truy cập đường dẫn sau để setup domain và port của instance production bạn setup trên server:
BE/config/environments/production/server.json

--------
Setup các business về socket.io của server ở đường dẫn sau:
BE/config/functions/bootstrap.js